// app2.js
new Vue({
    el: '#app2',
    data: {
      posts: [],
      searchQuery: '',
      filteredPosts: []
    },
    created: function () {
      this.fetchPosts();
      this.$on('postCreated', this.handlePostCreated);
    },
    methods: {
      fetchPosts: function () {
        this.$http.get('https://jsonplaceholder.typicode.com/posts')
          .then(response => {
            this.posts = response.body;
            this.filteredPosts = this.posts;
          })
          .catch(error => {
            console.error('Error:', error);
          });
      },
      searchPosts: function () {
        const query = this.searchQuery.toLowerCase();
        this.filteredPosts = this.posts.filter(post => {
          return (
            post.title.toLowerCase().includes(query) || 
            post.userId.toString().includes(query)
          );
        });
      },
      handlePostCreated: function (newPost) {
        this.posts.push(newPost);
      }
    }
  });
  
  // app3.js
  new Vue({
    el: '#app3',
    data: {
      newPost: {
        title: '',
        body: '',
        userId: 1
      }
    },
    methods: {
      createPost: function () {
        const post = Object.assign({}, this.newPost);
        this.$root.$emit('postCreated', post);
        this.newPost.title = '';
        this.newPost.body = '';
        this.newPost.userId = 1;
      }
    }
  });