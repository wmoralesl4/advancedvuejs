app.component('cards', {
    data(){
        return {
            cards: [
                {
                    title: 'Vue',
                    content: 'El mas facil de utilizar',
                    category: 'Framework'
                },
                {
                    title: 'Angular',
                    content: 'El mas dificlil de utilizar',
                    category: 'Framework'
                },
                {
                    title: 'React',
                    content: 'El mas popular del momento',
                    category: 'Framework'
                }
            ]
        }
    },
    methods:{
        removeCardHijo(context){
        const { title } = context
        const filtered= this.cards.filter(card => card.title != title)
        this.cards = filtered
        alert("Eliminando " + title)
    },
    createCard(context){
        const cardIfo = context
        console.log(cardIfo) 
        this.cards.push(cardIfo)
    }

    },
    template: `
    <card-form @cardCreated="createCard"></card-form>
    <br>
     <card @removeCardPadre="removeCardHijo" v-for="card of cards" :title="card.title" :content="card.content" :category="card.category"></card>

    `
}
)