app.component('card', {
    // props:['title', 'content', 'category'],
    props:{
        title:{
            type: String,
            required:true   
        },
        content:{
            type: String,
            required:true   
        },
        category:{
            type: String,
            required:true   
        },

    },
    methods:{
        removeCard(){
            this.$emit('removeCardPadre',{
                title: this.title
            })
        }
    },
    template: `
    <div class="card" style="width: 18rem;">
            <div class="card-body">
                  <h5 class="card-title">{{title}}</h5>
                  <p class="card-text">{{content}} - {{category}}</p>
                  <button @click="removeCard()" class="btn btn-danger btn-sm">Borrar tarjeta</button>
            </div>
    </div>`
})