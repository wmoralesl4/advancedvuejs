app.component('card-form',{
    data(){
        return{
            title: '',
            content: '',
            category: ''
        }
    },
    methods:{
        showData(){
            console.log({title:this.title,content:this.content,category:this.category})
        },
        addCard(){
            this.$emit('cardCreated', {
                title:this.title,
                content:this.content,
                category:this.category
            })
        }
    },
    template:`
        <form @submit.prevent="addCard()">
        <input v-model="title" class="form-control mb-3"  placeholder="Ingresa el titulo">
        <textarea v-model="content" class="form-control mb-3" placeholder="ingresa el contenido"></textarea>
        <input v-model="category" class="form-control mb-3" placeholder="ingresa una categoria">
        <input class="btn btn-success" type="submit" value="Agregar">
        </form>
    `
})