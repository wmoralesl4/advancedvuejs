const eventBus = new Vue();

new Vue({
  el: '#app2',
  data: {
    posts: [],
    searchQuery: '',
    filteredPosts: []
  },
  created: function () {
    this.fetchPosts();
    eventBus.$on('postCreated', this.handlePostCreated);
  },
  methods: {
    fetchPosts: function () {
      this.$http.get('https://jsonplaceholder.typicode.com/posts')
        .then(response => {
          this.posts = response.body;
          this.filteredPosts = this.posts;
        })
        .catch(error => {
          console.error('Error:', error);
        });
    },
    searchPosts: function () {
      const query = this.searchQuery.toLowerCase();
      this.filteredPosts = this.posts.filter(post => {
        return (
          post.title.toLowerCase().includes(query) || 
          post.userId.toString() === query
        );
      });
    },
    handlePostCreated: function (newPost) {
      this.posts.push(newPost);
    },
    editPost: function (post) {
        post.editing = true; 
        post.updatedTitle = post.title;
        post.updatedBody = post.body;
        this.searchPosts(); //Sin esto no funcionaba jajaja

        
    },

    savePost: function (post) {
      post.editing = false;
        post.title = post.updatedTitle;
      post.body = post.updatedBody;
        this.searchPosts();

    },

    cancelEdit: function (post) {
        post.editing = false; 
      this.searchPosts();

    }
  }
});



// app3.js
new Vue({
    el: '#app3',
    data: {
      newPost: {
        title: '',
        body: '',
        userId: 1
      },
      nextId: 101 
    },
    methods: {
      createPost: function () {
        const post = Object.assign({}, this.newPost);
        post.id = this.nextId; 
        this.nextId++; 

        eventBus.$emit('postCreated', post);
  
        this.newPost.title = '';
        this.newPost.body = '';
        this.newPost.userId = 1;
      }
    }
  });
