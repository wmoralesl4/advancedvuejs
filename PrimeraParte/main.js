const app = Vue.createApp({
data(){
    return {
        name: "Wilson",
        age: 22,
        role: 'Estudiante',
        photo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSpjZqIy6y4hik__DqDSDOkRJMEcPHdQKy-zA&s',
        photoWidth:200,
        frameworks: ['Angular', 'React', 'Vue']
    }
},
})

const dap = Vue.createApp({
data(){
    return {
        name: 'Segunda App',
        count: 0
    }
},
methods:{
    incrementCount(){
        this.count++;    
    }
}

})

const cap = Vue.createApp({
    data(){
        return {
            name: 'Frutas App',
            fruits: ['Banana', 'Apple', 'Orange', 'Coconut'],
            capacity: 8
        }
    },
    methods:{
        removeFruit(){
            this.fruits.pop();
        }
    },

    computed:{
        capacityLeft(){
            return this.capacity -  this.fruits.length
        }
    }
    
    })